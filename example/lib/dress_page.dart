import 'dart:math' as math;
import 'package:arkit_plugin/arkit_plugin.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:vector_math/vector_math_64.dart' as vector;

class DressPage extends StatefulWidget {
  @override
  _DressPageState createState() => _DressPageState();
}

class _DressPageState extends State<DressPage> {
  late ARKitController arkitController;
  ARKitReferenceNode? node;

  @override
  void dispose() {
    arkitController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('Dress Test')),
        body: Container(
          child: ARKitSceneView(
            enablePinchRecognizer: true,
            enablePanRecognizer: true,
            enableRotationRecognizer: true,
            showFeaturePoints: true,
            planeDetection: ARPlaneDetection.horizontal,
            onARKitViewCreated: onARKitViewCreated,
          ),
        ),
      );

  void onARKitViewCreated(ARKitController arkitController) {
    this.arkitController = arkitController;
    this.arkitController.onNodePinch = (pinch) => _onPinchHandler(pinch);
    this.arkitController.onNodePan = (pan) => _onPanHandler(pan);
    this.arkitController.onNodeRotation =
        (rotation) => _onRotationHandler(rotation);
    arkitController.addCoachingOverlay(CoachingOverlayGoal.horizontalPlane);
    arkitController.onAddNodeForAnchor = _handleAddAnchor;
  }

  void _onPinchHandler(List<ARKitNodePinchResult> pinch) {
    // final pinchNode = pinch.firstWhereOrNull(
    //       (e) => e.nodeName == node?.name,
    // );
    // if (pinchNode != null) {
    //   final scale = vector.Vector3.all(pinchNode.scale);
    //   node?.scale = scale;
    // }
  }

  void _onPanHandler(List<ARKitNodePanResult> pan) {
    final panNode = pan.firstWhereOrNull((e) => e.nodeName == 'Dress_test');
    if (panNode != null) {
      final rotation = node?.eulerAngles;
      rotation?.x += 0.05;
      node?.eulerAngles = rotation!;

      /// Наклон
      // final old = node?.eulerAngles;
      // final newAngleY = panNode.translation.x * math.pi / 180;
      // node?.eulerAngles =
      //     vector.Vector3(old?.x ?? 0, newAngleY, old?.z ?? 0);
    }
  }

  void _onRotationHandler(List<ARKitNodeRotationResult> rotation) {
    // final rotationNode = rotation.firstWhereOrNull(
    //       (e) => e.nodeName == node?.name,
    // );
    // if (rotationNode != null) {
    //   final rotation = node?.eulerAngles ??
    //       vector.Vector3.zero() + vector.Vector3.all(rotationNode.rotation);
    //   node?.eulerAngles = rotation;
    // }
  }
  
  void _handleAddAnchor(ARKitAnchor anchor) {
    if (anchor is ARKitPlaneAnchor) {
      _addPlane(arkitController, anchor);
    }
  }

  void _addPlane(ARKitController controller, ARKitPlaneAnchor anchor) {
    if (node != null) {
      controller.remove(node!.name);
    }
    node = ARKitReferenceNode(
      url: 'models.scnassets/Dress_Test.dae',
      scale: vector.Vector3.all(0.01),
    );
    controller.add(node!, parentNodeName: anchor.nodeName);
  }
}
